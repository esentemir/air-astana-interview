package com.airportauth.auth.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class LoginDto {
    @Schema(name = "username", example = "user1", description = "minimum size is 3 maximum 64 characters long")
    @Size(min = 3, max = 64)
    private String username;

    @Schema(name = "password", example = "12345", description = "Minimum size is 5")
    @Size(min = 5, max = 64)
    private String password;
}
