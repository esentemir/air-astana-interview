package com.airportauth.auth.dto.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class AuthResponse {
    private String authToken;
    private String refreshToken;

    public AuthResponse(String authToken) {
        this.authToken = authToken;
    }

    public AuthResponse(String authToken, String refreshToken) {
        this.authToken = authToken;
        this.refreshToken = refreshToken;
    }
}
