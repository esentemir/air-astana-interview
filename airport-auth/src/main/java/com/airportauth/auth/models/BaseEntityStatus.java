package com.airportauth.auth.models;

public enum BaseEntityStatus {
    DELETED,
    ACTIVE
}
