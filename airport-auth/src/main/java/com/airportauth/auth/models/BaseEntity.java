package com.airportauth.auth.models;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@MappedSuperclass
@Data
@NoArgsConstructor
@SuperBuilder
public class BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "is_deleted")
    @Enumerated(EnumType.STRING)
    protected BaseEntityStatus entityStatus;

    public BaseEntity(Long id, BaseEntityStatus entityStatus) {
        this.id = id;
        this.entityStatus = entityStatus;
    }

    public BaseEntity(BaseEntityStatus entityStatus) {
        this.entityStatus = entityStatus;
    }
}
