package com.airportauth.auth.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "airport_roles")
@Data
@NoArgsConstructor
public class Role extends BaseEntity {
    @Column(name = "code")
    private String code;

    public Role(String code) {
        super(BaseEntityStatus.ACTIVE);
        this.code = code;
    }
}
