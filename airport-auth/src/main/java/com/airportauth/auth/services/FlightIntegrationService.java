package com.airportauth.auth.services;


import com.airportauth.auth.security.JwtService;
import com.airportmanagement.airportcore.dto.request.ChangeStatusDto;
import com.airportmanagement.airportcore.dto.request.CreateFlightDto;
import com.airportmanagement.airportcore.dto.response.FlightDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Component
@Slf4j
@RequiredArgsConstructor
public class FlightIntegrationService {

    private final JwtService jwtService;
    private final RestTemplate restTemplate;
    @Value("${flight.core.baseurl}")
    private String baseUrl;

    public FlightDto createFlight(CreateFlightDto flight, String token) {
        var userName = jwtService.extractUsername(token);
        final var url = baseUrl;
        final var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        log.info("User : {}, creating flight with : {}", userName, flight);
        try {
            final var request = new HttpEntity<>(flight, headers);
            final var responseEntity = restTemplate.postForEntity(url, request, FlightDto.class);
            if (responseEntity.getStatusCode().isError()) {
                log.error("User : {}, error on saving {}", userName, flight);
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            if (responseEntity.hasBody()) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            log.error("User : {}, error on saving {}", jwtService.extractUsername(token), flight);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return null;
    }

    public List<FlightDto> getFlights(String origin, String destination) {
        final var url = baseUrl + "?origin={origin}&destination={destination}";

        try {
            final var responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<FlightDto>>() {
            }, origin, destination);
            if (responseEntity.getStatusCode().value() != 200) {
                log.error("Error while performing get flights");
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            if (responseEntity.hasBody()) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return null;
    }

    public FlightDto updateStatus(ChangeStatusDto dto, String token) {
        final var url = baseUrl + "/status";
        var userName = jwtService.extractUsername(token);

        log.info("User : {}, update flight status with : {}", userName, dto);
        try {
            final var request = new HttpEntity<>(dto, null);
            final var responseEntity = restTemplate.postForEntity(url, request, FlightDto.class);
            if (responseEntity.getStatusCode().isError()) {
                log.error("User : {}, update flight error with : {}", userName, dto);
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            if (responseEntity.hasBody()) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return null;
    }
}
