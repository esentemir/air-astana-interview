package com.airportauth.auth.services;


import com.airportauth.auth.dto.RegUserDto;
import com.airportauth.auth.models.User;

public interface UserService {

    User getByUserName(String userName);

    User findById(Long id);

    User register(RegUserDto dto);
}
