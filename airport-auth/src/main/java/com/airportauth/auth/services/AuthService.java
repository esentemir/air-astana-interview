package com.airportauth.auth.services;

import com.airportauth.auth.dto.LoginDto;
import com.airportauth.auth.dto.RegUserDto;
import com.airportauth.auth.dto.responses.AuthResponse;

public interface AuthService {
    AuthResponse authenticate(LoginDto loginDto);

    AuthResponse register(RegUserDto request);
}
