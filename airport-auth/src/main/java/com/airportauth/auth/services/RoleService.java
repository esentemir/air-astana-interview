package com.airportauth.auth.services;

import com.airportauth.auth.models.Role;

import java.util.Optional;

public interface RoleService {

    Role checkIfExistOrCreateNew(String roleName);
}
