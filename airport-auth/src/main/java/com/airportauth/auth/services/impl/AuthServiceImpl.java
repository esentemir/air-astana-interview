package com.airportauth.auth.services.impl;

import com.airportauth.auth.dto.LoginDto;
import com.airportauth.auth.dto.RegUserDto;
import com.airportauth.auth.dto.responses.AuthResponse;
import com.airportauth.auth.models.BaseEntityStatus;
import com.airportauth.auth.models.Role;
import com.airportauth.auth.models.User;
import com.airportauth.auth.repositories.UserRepository;
import com.airportauth.auth.security.JwtService;
import com.airportauth.auth.services.AuthService;
import com.airportauth.auth.services.RoleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.EntityNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;
    private final PasswordEncoder passwordEncoder;
    private final RoleService roleService;

    @Transactional
    public AuthResponse register(RegUserDto request) {
        Role role;
        if (request.getUsername().equals("Admin") || request.getUsername().equals("admin")) {
            role = roleService.checkIfExistOrCreateNew("MODERATOR");
        } else {
            role = roleService.checkIfExistOrCreateNew("USER");
        }

        var user = User.builder()
                .username(request.getUsername())
                .password(passwordEncoder.encode(request.getPassword()))
                .roles(role)
                .entityStatus(BaseEntityStatus.ACTIVE)
                .build();
        userRepository.save(user);
        var jwtToken = jwtService.generateToken(user);
        var refreshToken = jwtService.generateRefreshToken(user);

        return new AuthResponse(jwtToken, refreshToken);
    }

    public void refreshToken(
            HttpServletRequest request,
            HttpServletResponse response
    ) throws IOException {
        final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        final String refreshToken;
        final String username;

        if (authHeader == null ||!authHeader.startsWith("Bearer ")) {
            return;
        }
        refreshToken = authHeader.substring(7);
        username = jwtService.extractUsername(refreshToken);
        if (username != null) {
            var user = userRepository.findByUsername(username)
                    .orElseThrow();
            if (jwtService.isTokenValid(refreshToken, user)) {
                var accessToken = jwtService.generateToken(user);

                var authResponse = new AuthResponse(accessToken, refreshToken);
                new ObjectMapper().writeValue(response.getOutputStream(), authResponse);
            }
        }
    }


    @Override
    public AuthResponse authenticate(LoginDto loginDto) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginDto.getUsername(),
                        loginDto.getPassword()
                )
        );
        var user = userRepository.findByUsername(loginDto.getUsername()).orElseThrow(EntityNotFoundException::new);

        var token = jwtService.generateToken(user);

        return new AuthResponse(token);
    }
}
