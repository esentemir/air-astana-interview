package com.airportauth.auth.services.impl;

import com.airportauth.auth.dto.RegUserDto;
import com.airportauth.auth.models.User;
import com.airportauth.auth.repositories.RolesRepository;
import com.airportauth.auth.repositories.UserRepository;
import com.airportauth.auth.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RolesRepository rolesRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User getByUserName(String userName) {
        return null;
    }

    @Override
    public User findById(Long id) {
        return null;
    }

    @Override
    public User register(RegUserDto dto) {
        return null;
    }
}
