package com.airportauth.auth.services.impl;

import com.airportauth.auth.models.Role;
import com.airportauth.auth.repositories.RolesRepository;
import com.airportauth.auth.services.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RolesRepository repository;

    @Override
    @Transactional
    public Role checkIfExistOrCreateNew(String roleName) {
        var role = repository.findByCode(roleName).stream().findFirst();

        return role.orElseGet(() -> repository.save(new Role(roleName)));

    }
}
