package com.airportauth.auth.controllers;

import com.airportauth.auth.services.FlightIntegrationService;
import com.airportmanagement.airportcore.dto.request.ChangeStatusDto;
import com.airportmanagement.airportcore.dto.request.CreateFlightDto;
import com.airportmanagement.airportcore.dto.response.FlightDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/moderator/flights")
@RequiredArgsConstructor
public class ModeratorController {

    private final FlightIntegrationService integrationService;

    @Operation(
            description = "Endpoint for creating a new Flight Object or to update an existing one",
            summary = "The data is stored in db and cache",
            responses = {
                    @ApiResponse(description = "Success", responseCode = "200"),
                    @ApiResponse(description = "Internal server error", responseCode = "500"),
                    @ApiResponse(description = "Data validation exception", responseCode = "400")
            })
    @PreAuthorize("hasRole('MODERATOR')")
    @PostMapping(value = "")
    public ResponseEntity<FlightDto> createFlight(@RequestHeader("Authorization") String token,
                                                  @Valid @RequestBody CreateFlightDto dto) {
        return ResponseEntity.ok(integrationService.createFlight(dto, token));
    }

    @Operation(
            description = "Endpoint for extracting data and filter it by params",
            summary = "The data is evicted from cache",
            responses = {
                    @ApiResponse(description = "Success", responseCode = "200"),
                    @ApiResponse(description = "Internal server error", responseCode = "500"),
                    @ApiResponse(description = "Data validation exception", responseCode = "400")
            })
    @PreAuthorize("hasAuthority('ROLE_MODERATOR')")
    @GetMapping(value = "")
    public ResponseEntity<List<FlightDto>> filterFlights(@RequestParam(name = "origin", required = false, defaultValue = "") String origin,
                                                         @RequestParam(name = "destination", required = false, defaultValue = "") String destination) {
        return ResponseEntity.ok(integrationService.getFlights(origin, destination));
    }

    @Operation(
            description = "Endpoint for updating existing flight status",
            summary = "Change status on one of the following: [ InTime, Delayed, Cancelled ]",
            responses = {
                    @ApiResponse(description = "Success", responseCode = "200"),
                    @ApiResponse(description = "Internal server error", responseCode = "500"),
                    @ApiResponse(description = "Data validation exception", responseCode = "400")
            })
    @PreAuthorize("hasAuthority('ROLE_MODERATOR')")
    @PostMapping(value = "/status")
    public ResponseEntity<FlightDto> changeStatus(@RequestHeader("Authorization") String token,
                                                  @RequestBody ChangeStatusDto dto) {
        return ResponseEntity.ok(integrationService.updateStatus(dto, token));
    }
}
