package com.airportmanagement.airportcore.services.impl;

import com.airportmanagement.airportcore.dto.response.FlightDto;
import com.airportmanagement.airportcore.models.FlightStatus;
import com.airportmanagement.airportcore.services.CacheService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class FlightServiceImplTest {

    @Mock
    private CacheService cacheService;

    @InjectMocks
    private FlightServiceImpl flightService;

    @Test
    public void shouldFilterByOrigin() {
        Mockito.when(cacheService.getAllFlights()).thenReturn(getFlights());
        var expectedResult = getExpectedByOriginResult();

        var result = flightService.filter("ALA", "");

        Assertions.assertNotNull(result);
        Assertions.assertEquals(2, result.size());
        Assertions.assertEquals(result.get(0).getId(), expectedResult.get(0).getId());
        Assertions.assertEquals(result.get(1).getId(), expectedResult.get(1).getId());
    }


    @Test
    public void shouldFilterByDestination() {
        Mockito.when(cacheService.getAllFlights()).thenReturn(getFlights());
        var expectedResult = getExpectedByDestinationResult();

        var result = flightService.filter("", "NQZ");

        Assertions.assertNotNull(result);
        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(result.get(0).getId(), expectedResult.get(0).getId());
    }

    private List<FlightDto> getExpectedByDestinationResult() {
        var res = new ArrayList<FlightDto>();
        res.add(
                new FlightDto(
                        1L,
                        "ALA",
                        "NQZ",
                        OffsetDateTime.parse("2023-09-18T01:30:00.0000000+06:00"),
                        OffsetDateTime.parse("2023-09-18T03:00:00.0000000+06:00"),
                        FlightStatus.InTime));
        return res;
    }

    private List<FlightDto> getExpectedByOriginResult() {
        var res = new ArrayList<FlightDto>();
        res.add(
                new FlightDto(
                        1L,
                        "ALA",
                        "NQZ",
                        OffsetDateTime.parse("2023-09-18T01:30:00.0000000+06:00"),
                        OffsetDateTime.parse("2023-09-18T03:00:00.0000000+06:00"),
                        FlightStatus.InTime));
        res.add(
                new FlightDto(
                        2L,
                        "ALA",
                        "ANT",
                        OffsetDateTime.parse("2023-09-19T16:10:00.0000000+06:00"),
                        OffsetDateTime.parse("2023-09-20T00:30:00.0000000+03:00"),
                        FlightStatus.InTime));
        return res;
    }

    private List<FlightDto> getFlights() {
        var res = new ArrayList<FlightDto>();
        res.add(new FlightDto(
                1L,
                "ALA",
                "NQZ",
                OffsetDateTime.parse("2023-09-18T01:30:00.0000000+06:00"),
                OffsetDateTime.parse("2023-09-18T03:00:00.0000000+06:00"),
                FlightStatus.InTime));
        res.add(new FlightDto(
                2L,
                "ALA",
                "ANT",
                OffsetDateTime.parse("2023-09-19T16:10:00.0000000+06:00"),
                OffsetDateTime.parse("2023-09-20T00:30:00.0000000+03:00"),
                FlightStatus.InTime));
        res.add(new FlightDto(
                3L,
                "NYC",
                "ALA",
                OffsetDateTime.parse("2023-09-20T14:30:00.0000000+12:00"),
                OffsetDateTime.parse("2023-09-21T14:00:00.0000000+06:00"),
                FlightStatus.InTime));
        return res;
    }
}
