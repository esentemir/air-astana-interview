package com.airportmanagement.airportcore.controllers;

import com.airportmanagement.airportcore.dto.request.ChangeStatusDto;
import com.airportmanagement.airportcore.dto.request.CreateFlightDto;
import com.airportmanagement.airportcore.dto.response.FlightDto;
import com.airportmanagement.airportcore.services.FlightService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/flight")
public class FlightController {

    private final FlightService flightService;


    @PostMapping(value = "")
    public ResponseEntity<FlightDto> createFlight(@Valid @RequestBody CreateFlightDto dto) {
        return ResponseEntity.ok(flightService.createFlight(dto));
    }


    @GetMapping(value = "")
    public ResponseEntity<List<FlightDto>> filterFlights(@RequestParam(name = "origin", required = false) String origin,
                                                         @RequestParam(name = "destination", required = false) String destination) {
        return ResponseEntity.ok(flightService.filter(origin, destination));
    }

    @PostMapping(value = "/status")
    public ResponseEntity<FlightDto> changeStatus(@RequestBody ChangeStatusDto dto) {
        return ResponseEntity.ok(flightService.updateStatus(dto));
    }
}
