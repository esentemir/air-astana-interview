package com.airportmanagement.airportcore.services.impl;

import com.airportmanagement.airportcore.dto.request.ChangeStatusDto;
import com.airportmanagement.airportcore.dto.request.CreateFlightDto;
import com.airportmanagement.airportcore.dto.response.FlightDto;
import com.airportmanagement.airportcore.repos.FlightRepository;
import com.airportmanagement.airportcore.services.CacheService;
import com.airportmanagement.airportcore.services.FlightService;
import com.airportmanagement.airportcore.services.FlightsFilter;
import com.airportmanagement.airportcore.services.MappingUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FlightServiceImpl implements FlightService {

    private final FlightRepository flightRepository;
    private final CacheService cacheService;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Modifying
    @CachePut(value = "flights", key = "#result.id")
    public FlightDto createFlight(CreateFlightDto dto) {
        var savedFlight = flightRepository.save(MappingUtils.toFlight(dto));
        return MappingUtils.toFlightDto(savedFlight);
    }

    @Override
    public List<FlightDto> filter(String origin, String destination) {
        var flights = cacheService.getAllFlights();

        var filter = new FlightsFilter(flights);
        if (destination.isBlank() && origin.isBlank())
            return filter.getFlights();

        if (!destination.isBlank() && !origin.isBlank()) {
            return filter.filterByOriginAndDestionation(origin, destination);
        } else {
            if (destination.isBlank())
                return filter.filterByOrigin(origin);
            else
                return filter.filterByDestination(destination);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Modifying
    @CachePut(value = "flights", key = "#result.id")
    public FlightDto updateStatus(ChangeStatusDto dto) {
        var flight = flightRepository.findById(dto.getId()).orElseThrow(IllegalArgumentException::new);

        flight.setFlightStatus(dto.getFlightStatus());

        var updatedFlight = flightRepository.save(flight);

        return MappingUtils.toFlightDto(updatedFlight);
    }

    @Cacheable(value = "flights")
    @EventListener(ApplicationReadyEvent.class)
    public List<FlightDto> cacheAllExistingFlights() {
        return flightRepository.findAll().stream().map(MappingUtils::toFlightDto).collect(Collectors.toList());
    }
}
