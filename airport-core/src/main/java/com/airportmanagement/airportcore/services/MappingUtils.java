package com.airportmanagement.airportcore.services;

import com.airportmanagement.airportcore.dto.request.CreateFlightDto;
import com.airportmanagement.airportcore.dto.response.FlightDto;
import com.airportmanagement.airportcore.models.Flight;
import lombok.experimental.UtilityClass;

import java.time.OffsetDateTime;

@UtilityClass
public class MappingUtils {

    public static Flight toFlight(CreateFlightDto dto) {
        var result = new Flight();
        result.setOrigin(dto.getOrigin());
        result.setDestination(dto.getDestination());
        result.setDeparture(OffsetDateTime.parse(dto.getDeparture()));
        result.setArrival(OffsetDateTime.parse(dto.getArrival()));
        result.setFlightStatus(dto.getFlightStatus());
        return result;
    }

    public static FlightDto toFlightDto(Flight model) {
        return new FlightDto(model.getId(),
                model.getOrigin(),
                model.getDestination(),
                model.getDeparture(),
                model.getArrival(),
                model.getFlightStatus());
    }
}
