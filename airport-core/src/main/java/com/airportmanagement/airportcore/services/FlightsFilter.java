package com.airportmanagement.airportcore.services;

import com.airportmanagement.airportcore.dto.response.FlightDto;
import lombok.Getter;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class FlightsFilter {

    private final List<FlightDto> flights;

    public FlightsFilter(List<FlightDto> flightDtoList) {
        this.flights = sortByArrivalTime(flightDtoList);
    }

    public List<FlightDto> filterByOrigin(String origin) {
        return flights.stream().filter(flight -> flight.getOrigin().equals(origin))
                .collect(Collectors.toList());
    }

    public List<FlightDto> filterByDestination(String destination) {
        return flights.stream().filter(flight -> flight.getDestination().equals(destination))
                .collect(Collectors.toList());
    }

    public List<FlightDto> filterByOriginAndDestionation(String origin, String destination) {
        return flights.stream().filter(flight -> flight.getOrigin().equals(origin) && flight.getDestination().equals(destination))
                .collect(Collectors.toList());
    }

    private List<FlightDto> sortByArrivalTime(List<FlightDto> flights) {
        flights.sort(new Comparator<FlightDto>() {
            @Override
            public int compare(FlightDto o1, FlightDto o2) {
                return o1.getArrival().compareTo(o2.getArrival());
            }
        });
        return flights;
    }
}
