package com.airportmanagement.airportcore.services;

import com.airportmanagement.airportcore.dto.response.FlightDto;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class CacheService {

    private final CacheManager cacheManager;

    public List<FlightDto> getAllFlights() {
        var caffeineCacheManager = (CaffeineCacheManager) cacheManager;
        var cache = (CaffeineCache) caffeineCacheManager.getCache("flights");
        assert cache != null;

        var caffeine = cache.getNativeCache().asMap();
        var list = new ArrayList<FlightDto>();

        for (Map.Entry<Object, Object> entrySet : caffeine.entrySet()) {
            // Filling cache with data already existing in database
            if (entrySet.getValue() instanceof ArrayList) {
                list.addAll((ArrayList<FlightDto>) entrySet.getValue());
            } else {
                // In case data was stored manually after application startup
                replaceExistingWithNewOrAdd(list, (FlightDto) entrySet.getValue());
            }
        }

        return list;
    }

    private ArrayList<FlightDto> replaceExistingWithNewOrAdd(ArrayList<FlightDto> extractedFromDb, FlightDto recentlyCached) {
        var found = extractedFromDb.stream().filter(dto -> dto.getId().compareTo(recentlyCached.getId()) == 0).findFirst();

        if (found.isEmpty()) {
            extractedFromDb.add(recentlyCached);
            return extractedFromDb;
        }

        extractedFromDb.remove(found.get());
        extractedFromDb.add(recentlyCached);

        return extractedFromDb;
    }
}
