package com.airportmanagement.airportcore.services;

import com.airportmanagement.airportcore.dto.request.ChangeStatusDto;
import com.airportmanagement.airportcore.dto.request.CreateFlightDto;
import com.airportmanagement.airportcore.dto.response.FlightDto;

import java.util.List;

public interface FlightService {

    FlightDto createFlight(CreateFlightDto dto);

    List<FlightDto> filter(String origin, String destination);

    FlightDto updateStatus(ChangeStatusDto dto);
}
