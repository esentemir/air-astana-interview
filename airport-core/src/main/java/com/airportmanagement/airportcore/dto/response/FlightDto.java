package com.airportmanagement.airportcore.dto.response;

import com.airportmanagement.airportcore.models.FlightStatus;
import lombok.Getter;

import java.time.OffsetDateTime;

@Getter
public final class FlightDto {
    private final Long id;
    private final String origin;
    private final String destination;
    private final OffsetDateTime departure;
    private final OffsetDateTime arrival;
    private final FlightStatus flightStatus;

    public FlightDto(Long id, String origin, String destination, OffsetDateTime departure, OffsetDateTime arrival, FlightStatus status) {
        this.id = id;
        this.origin = origin;
        this.destination = destination;
        this.departure = departure;
        this.arrival = arrival;
        this.flightStatus = status;
    }
}
