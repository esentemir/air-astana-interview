package com.airportmanagement.airportcore.dto.request;

import com.airportmanagement.airportcore.models.FlightStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Valid
public final class CreateFlightDto {
    @Schema(name = "Airport departure name", example = "ALA")
    @Size(min = 3, max = 3, message = "Origin field must be 3 characters long")
    private String origin;

    @Schema(name = "Airport destination name", example = "NQZ")
    @Size(min = 3, max = 3, message = "Destination field must be 3 characters long")
    private String destination;

    @Schema(name = "Departure time from origin", example = "")
    private String departure;

    @Schema(name = "Arrival time from destination", example = "")
    private String arrival;

    @Schema(name = "Flight status : [ InTime, Delayed, Cancelled ]", example = "InTime")
    private FlightStatus flightStatus;

    @Override
    public String toString() {
        return String.format("CreateFlightDto (origin = %s, destination = %s, departure = %s, arrival = %s, flightStatus = %s)",
                origin, destination, departure, arrival, flightStatus.name());
    }
}
