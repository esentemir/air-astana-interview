package com.airportmanagement.airportcore.dto.request;

import com.airportmanagement.airportcore.models.FlightStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChangeStatusDto {
    @Schema(name = "id", example = "252", description = "Flight object's id")
    private Long id;

    @Schema(name = "flightStatus", example = "[InTime, Delayed, Cancelled]", description = "new flight status")
    private FlightStatus flightStatus;

    @Override
    public String toString() {
        return String.format("ChangeStatusDto (id = %s, flightStatus = %s)", id, flightStatus.name());
    }
}
