package com.airportmanagement.airportcore.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "airport_flight")
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "origin")
    private String origin;

    @Column(name = "destination")
    private String destination;

    @Column(name = "departure")
    private OffsetDateTime departure;

    @Column(name = "arrival")
    private OffsetDateTime arrival;

    @Column(name = "flightStatus")
    @Enumerated(value = EnumType.STRING)
    private FlightStatus flightStatus;
}
