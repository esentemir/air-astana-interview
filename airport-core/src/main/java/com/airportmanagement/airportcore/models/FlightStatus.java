package com.airportmanagement.airportcore.models;

public enum FlightStatus {
    InTime, Delayed, Cancelled;
}
