package com.airportmanagement.airportcore.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
        info = @Info(
                contact = @Contact(
                        name = "Iskander Akhmedov",
                        email = "Iskand00@gmail.com"),
                description = "Documentation for Flight service",
                title = "Flight service documentation",
                version = "1.0",
                termsOfService = "Terms of service"
        ),
        servers = {@Server(
                description = "Develop",
                url = "http://localhost:9090")}

)
public class SwaggerConfig {
}
