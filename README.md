# air-astana-interview



## Project Structure

Current project is a multi repository
Spring boot: 3.2.0

***

## Name
Airport service for an interview

## Description

This is spring boot microservice based application.
All request should go through auth-microservice in order to gain jwt token.

Documentation implemented by using Swagger API. Also Postman collection will be included.

## Tips to use

In order to create a moderator user. You should just simply register a user with "Admin" username.
